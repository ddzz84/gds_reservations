<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function usedSlot($day){
      return DB::select("SELECT slot
        FROM booking
        WHERE `date` = ?",[$day]);
    }

    public function checkUserBooking($userId){
      return DB::select("SELECT slot
        FROM booking
        WHERE `user_id` = ?",[$userId]);
    }


}



/*

agents cont : 3
slot num: 3
  a1    a1    a1
|   | |   | |   |
|   | |   | |   |
|   | |   | |   |


 */
