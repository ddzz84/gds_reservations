<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function getUserAnsers($userId){
      return DB::select("SELECT a.answer, q.label
        FROM answers a INNER JOIN questions q ON a.question_id = q.id
        WHERE `user_id` = ?",[$userId]);

    }
}
