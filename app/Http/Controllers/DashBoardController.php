<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Booking;
use App\Answer;

class DashBoardController extends Controller
{
  public function index(){

    if(Auth::user()->type == 'user')
      return $this->user();
    else
      return $this->agent();
  }
    //
    private function agent()
    {
        return view('dashboard_agent');
    }

    private function user()
    {
      $booking = Booking::where('user_id',Auth::user()->id)->get();

      if(!$booking->isEmpty())
        return view('user.profile');

      $questionary = Answer::where('user_id',Auth::user()->id)->get();

        if($questionary->isEmpty()){
          return view('user.questions');
        }else{
          return view('user.booking');
        }
    }
}
